一个低代码平台的前端部分，靠拖拉拽生成页面。
## 功能点
这是本项目具有的功能点，如果想了解详情请参考本项目的三篇文档，每个功能点都有不同程度的描述以及动图帮助你理解。
1. 编辑器
2. 自定义组件
3. 拖拽
4. 删除组件、调整图层层级
5. 放大缩小
6. 撤消、重做
7. 组件属性设置
8. 吸附
9. 预览、保存代码
10. 绑定事件
11. 绑定动画（支持动画时长，是否循环等配置）
12. 导入 PSD
13. 手机模式
14. 拖拽旋转
15. 复制粘贴剪切
16. 数据交互
17. 发布
18. 多个组件的组合和拆分
19. 文本组件
20. 矩形组件
21. 锁定组件
22. 快捷键
23. 网格线
24. 编辑器快照的另一种实现方式

## 在线 DEMO
* [预览入口（Github）不翻墙可能速度慢](https://woai3c.github.io/visual-drag-demo)

## 文档
* [可视化拖拽组件库一些技术要点原理分析](https://github.com/woai3c/Front-end-articles/issues/19)
* [可视化拖拽组件库一些技术要点原理分析（二）](https://github.com/woai3c/Front-end-articles/issues/20)
* [可视化拖拽组件库一些技术要点原理分析（三）](https://github.com/woai3c/Front-end-articles/issues/21)

## License
MIT

## 自定义组件
1.在custom-component目录下创建自己的xxx.vue文件 如 VElcarousel
2.在index.js需要将改组件注册  
 注册路径 => Vue.component(key, () => import(`@/custom-component/${key}`)) 其中key为组件名称 具体路径组件可按实际情况进行修改
3.第三步在component-list.js 配置好改组件的参数及默认样式  label:组件名称, propValue:组件默认设置的传递参数,具体类型可以修改
4.新增组件的传递参数；如轮播图中的swiperList 由于默认只设置了propValue为传递参数，实际操作过程中是不够的。需要在components/index.vue 如下
            <component
                :is="item.component"
                v-if="item.component != 'v-text'"
                :id="'component' + item.id"
                class="component"
                :style="getComponentStyle(item.style)"
                :prop-value="item.propValue"
                :swiper-list="item.swiperList"
                :element="item"
            />
  
 在components/AttrList.vue 对其进行双向配置 //具体配置可以根据需求自定义配置
            <el-form-item v-if="curComponent.swiperList" label="轮播图设置">
                    <div style="clear:both" v-for="(item,i) of curComponent.swiperList" :key="i">
                        <div>第{{i+1}}张</div>
                        <el-input  v-model="item.url" type="textarea" />
                    </div>
        </el-form-item>          
 如果是一般传递参数比如字符串或者简单{key：value}对象用propValue就够了。由于propValue是默认为该组件的内容text,当为对象或者其他形式时需要对其进行修改
如：components/AttrList.vue
     <el-form-item v-if="curComponent && !excludes.includes(curComponent.component)" label="内容">
            <!-- 这里做了类型的判断，具体情况可以在实际项目中更改 -->
                <div v-if="typeof curComponent.propValue=='object'">
                  <el-input v-model="curComponent.propValue.value" type="textarea" />
                </div>
               <div v-else>
                 <el-input v-model="curComponent.propValue" type="textarea" />
              </div>
    </el-form-item>

## 赞助
![](https://github.com/woai3c/nand2tetris/blob/master/img/wx.jpg)
![](https://github.com/woai3c/nand2tetris/blob/master/img/zfb.jpg)
