/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
 export function isExternal(path) {
    return /^(https?:|mailto:|tel:)/.test(path)
  }
  
  // 对象字典序排序
  export function ObjSort(arys) {
    var newkey = Object.keys(arys).sort()
    var newObj = {}
    for (var i = 0; i < newkey.length; i++) {
      newObj[newkey[i]] = arys[newkey[i]]
    }
    return newObj
  }
  // 对象安装%2f拼接key=value
  export function ObjSlice(data) {
    let newdatastr = ''
    Object.keys(data).forEach(
      (item) => (newdatastr += `${item}=${data[item]}%2f`)
    )
    return newdatastr.slice(0, -3)
  }
  
  // 验证码规则
  export function validateCode(rule, value, callback) {
    if (value === '') {
      callback(new Error('请输入验证码'))
    }
    if (value.length != 6) {
      callback(new Error('请输入6位验证码'))
    }
    const pattern = new RegExp(/^[0-9]\d*$/)
    if (!pattern.test(value)) {
      callback(new Error('请输入纯数字的验证码'))
    }
    callback()
  }
  
  // 输入手机号规则
  export function validatePhone(rule, value, callback) {
    if (value === '') {
      callback(new Error('请输入手机号'))
    }
    if (value.length != 11) {
      callback(new Error('请输入11位手机号'))
    }
    const pattern1 = new RegExp(/^[0-9]\d*$/)
    if (!pattern1.test(value)) {
      callback(new Error('手机号格式错误!'))
    }
    const pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！%@#￥……&*（）——|{}【】‘；：”“'。，、？ ]")
    if (pattern.test(value)) {
      callback(new Error('手机号不能包含特殊字符'))
    }
    callback()
  }
  
  // 输入手机号规则
  export function validatePhoneReg(value) {
    if (value === '') {
      return false
    }
    if (value.length != 11) {
      return false
    }
    const pattern1 = new RegExp(/^[0-9]\d*$/)
    if (!pattern1.test(value)) {
      return false
    }
    const pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！%@#￥……&*（）——|{}【】‘；：”“'。，、？ ]")
    if (pattern.test(value)) {
      return false
    }
    return true
  }
  
  export function Notempty(value) {
    if (value === '') {
         return false
    }
    return true
  }
  export function validateEmail( value) {
    if (value === '') {
         return false
    }
    const pattern1 = new RegExp(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/)
    if (!pattern1.test(value)) {
      return false
    }
    return true
  }

  // 特殊字符规则
  export function validateCharacter(rule, value, callback) {
    const pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！%@#￥……&*（）——|{}【】‘；：”“'。，、？ ]")
    if (pattern.test(value)) {
      callback(new Error('不能包含特殊字符'))
    }
    callback()
  }
  
  // 特殊字符规则+非汉字
  export function validateCreditCode(rule, value, callback) {
    const reg = new RegExp('[\\u4E00-\\u9FFF]+', 'g')
    if (reg.test(value)) {
      callback(new Error('输入格式不正确! 不能包含中文'))
    }
    const pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！%@#￥……&*（）——|{}【】‘；：”“'。，、？ ]")
    if (pattern.test(value)) {
      callback(new Error('不能包含特殊字符'))
    }
    callback()
  }
  

  
  // 输入密码规则
  export function validatePass(rule, value, callback) {
    if (value === '') {
      callback(new Error('请输入密码'))
    }
    if (value.length > 20 || value.length < 8) {
      callback(new Error('密码长度在 8 到 20 个字符'))
    }
    const reg1 = new RegExp(/^(?![^a-zA-Z]+$)(?!\D+$)/)
    if (!reg1.test(value)) {
      callback(new Error('密码应包含字母加数字'))
    }
    const pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！%@#￥……&*（）——|{}【】‘；：”“'。，、？ ]")
    if (pattern.test(value)) {
      callback(new Error('密码不能包含特殊字符'))
    }
    callback()
  }
  
  export function pdfDownload(Name, url) {
    const fileName = Name
    const reg = /^([hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\/])+$/
    if (!reg.test(url)) {
      throw new Error('传入参数不合法,不是标准的文件链接')
    } else {
      const xhr = new XMLHttpRequest()
      xhr.open('get', url, true)
      xhr.responseType = 'blob'
      xhr.onload = function() {
        if (xhr.status == 200) {
          // 接受二进制文件流
          var blob = this.response
          downloadExportFile(blob, fileName)
        }
      }
      xhr.send()
    }
  }
  
  function downloadExportFile(blob, tagFileName) {
    const downloadElement = document.createElement('a')
    let href = blob
    if (typeof blob == 'string') {
      downloadElement.target = '_blank'
    } else {
      href = window.URL.createObjectURL(blob) // 创建下载的链接
    }
    downloadElement.href = href
    downloadElement.download =
        tagFileName +
        // 下载后文件名
    document.body.appendChild(downloadElement)
    downloadElement.click() // 点击下载
    document.body.removeChild(downloadElement) // 下载完成移除元素
    if (typeof blob != 'string') {
      window.URL.revokeObjectURL(href) // 释放掉blob对象
    }
  }
  
  /**
   * @param {string} str
   * @returns {Boolean}
   */
  export function validUsername(str) {
    const valid_map = ['admin', 'editor']
    return valid_map.indexOf(str.trim()) >= 0
  }
  
  /**
   * @param {string} url
   * @returns {Boolean}
   */
  export function validURL(url) {
    const reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
    return reg.test(url)
  }
  
  /**
   * @param {string} str
   * @returns {Boolean}
   */
  export function validLowerCase(str) {
    const reg = /^[a-z]+$/
    return reg.test(str)
  }
  
  /**
   * @param {string} str
   * @returns {Boolean}
   */
  export function validUpperCase(str) {
    const reg = /^[A-Z]+$/
    return reg.test(str)
  }
  
  /**
   * @param {string} str
   * @returns {Boolean}
   */
  export function validAlphabets(str) {
    const reg = /^[A-Za-z]+$/
    return reg.test(str)
  }
  
  /**
   * @param {string} email
   * @returns {Boolean}
   */
  export function validEmail(email) {
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return reg.test(email)
  }
  
  /**
   * @param {string} str
   * @returns {Boolean}
   */
  export function isString(str) {
    if (typeof str === 'string' || str instanceof String) {
      return true
    }
    return false
  }
  
  /**
   * @param {Array} arg
   * @returns {Boolean}
   */
  export function isArray(arg) {
    if (typeof Array.isArray === 'undefined') {
      return Object.prototype.toString.call(arg) === '[object Array]'
    }
    return Array.isArray(arg)
  }
  