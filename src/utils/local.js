/**
 * 本地存储操作封装
 * created by lxc 2020-06-24
 */

//  暴露
export default {
  get(key) {
    return JSON.parse(JSON.stringify(window.localStorage.getItem(key)))
  },
  set(key, value) {
    if (
      ['[object Object]', '[object Array]'].includes(
        Object.prototype.toString.call(value)
      )
    ) {
      window.localStorage.setItem(key, JSON.stringify(value))
    } else {
      window.localStorage.setItem(key, value)
    }
  },

  //获取token
  getCookie(cname) {
    if (cname === "") {
      return document.cookie
    }
    let newcookie = document.cookie.replace(" ", "").split(';');
    var value = ""
    for (let item in newcookie) {
      if (newcookie[item].indexOf(cname) > -1) {
        let stringkey = cname + "="
        value = newcookie[item].replace(stringkey, "")
      }
    }
    return value
    // let name = cname + '='
    // let ca = window.document.cookie.split(';')
    // for (let i = 0; i < ca.length; i++) {
    //   let c = ca[i].trim()
    //   if (c.indexOf(name) === 0) return c.substring(name.length, c.length)
    // }
    // return ''
  },
  remove(key) {
    window.localStorage.removeItem(key)
  },
  clear() {
    window.localStorage.clear()
  },
}
