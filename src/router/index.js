import Vue from 'vue'
import Router from 'vue-router'
// 获取原型对象上的push函数
const originalPush = Router.prototype.push
// 修改原型对象中的push方法
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(Router)

const routes = [
    {
        path: '/',
        name: 'index',
        component: () => import('@/views/index'),
    },
    {
        path: '/website',
        name: 'website',
        component: () => import('@/views/website'),
    },
    {
        path: '/preview',
        name: 'preview',
        component: () => import('@/components/Editor/Preview'),
    },
    {
        path: '/MobilePreview',
        name: 'MobilePreview',
        component: () => import('@/components/Editor/MobilePreview'),
    },
    {
        path: '/phoneSite',
        name: 'phoneSite',
        component: () => import('@/views/PhoneSite'),
    },
]

export default new Router({
    routes,
})
