// 公共样式

function getQueryUrlParamVal(name) {
    if (location.href.indexOf("?") == -1 || location.href.indexOf(name + '=') == -1) {
        return '';
    }
    var queryString = location.href.substring(location.href.indexOf("?") + 1);
    queryString = decodeURI(queryString);
    var parameters = queryString.split("&");
    var pos, paraName, paraValue;
    for (var i = 0; i < parameters.length; i++) {
        pos = parameters[i].indexOf('=');
        if (pos == -1) {
            continue;
        }
        paraName = parameters[i].substring(0, pos);
        paraValue = parameters[i].substring(pos + 1);
        if (paraName == name) {
            return unescape(paraValue.replace(/\+/g, " "));
        }
    }
    return '';
}
let type=getQueryUrlParamVal('type')
export const commonStyle = {
    rotate: 0,
    opacity: 1,
}

export const commonAttr = {
    animations: [],
    events: {},
    groupStyle: {}, // 当一个组件成为 Group 的子组件时使用
    isLock: false, // 是否锁定组件
}
import phoneConponentsList from './assembly/phoneConponentsList.js'
import customDataList from './assembly/customComponents.js'



// 编辑器左侧组件列表
const list = [
    {
        component: 'v-text',
        label: '文字',
        propValue: '双击编辑文字',
        icon: 'wenben',
        hoverClass:'',
        type:'CommonlyUsed',
        style: {
            width: 200,
            height: 22,
            fontSize: 14,
            fontWeight: 500,
            lineHeight: '',
            letterSpacing: 0,
            fontFamily:'Microsoft Yahei',
            textAlign: 'center',
            color: '',
            isGradient: false,
            textColor: ' #33AEAD',
            textColor2: '#FF9D19',
            deg:'-90'
        },
    },
    {
        component: 'v-button',
        label: '按钮',
        type:'CommonlyUsed',
        propValue: {value:'按钮',name:'customButton',indexedDB:1,themebtnColor:'rgb(7, 108, 224)',themeBtnClass:'v-button',
        themeBtn_gradient_color:'rgba(7, 108, 224,0.5)',
        btnthemeHover:'',
        link:'',
        target:false,
        radius:2,
        opacity:100,
        is_icon:false,
        icon:'https://2.ss.faisys.com/image/materialLib/illustration/000077.png',
        iconwidth:24,
        is_background:true,
        is_backType:1,
        btnBackground:'//1.s140i.faiusr.com/2/AIwBCAAQAhgAIJv17NUFKPLrqr8BMIAPOIAK.jpg',
        is_border:false,
        borderWidth:0,
        borderColor:'rgb(254, 87, 34)',
        bordeHoverColor:'rgba(0,0,0,0)',
        shadowColor:'#ddd',
        Yshadow:0,
        Xshadow:0,
        shadow:0,
        is_shadow:false,
      },
        isDialog:true,
        icon: 'button',
        style: {
            width: 110,
            height: 40,
            borderWidth: 1,
            borderColor: '',
            fontSize: 14,
            fontWeight: 500,
            lineHeight: '',
            letterSpacing: 0,
            textAlign: '',
            color: '',
            backgroundColor: '',
            top: 0,
            left: 0
        },
    },
    {
        component: 'v-input',
        label: '输入框',
        icon: 'wenben',
        type:'CommonlyUsed',
        isDialog:true,
        propValue: {value:'输入框',name:'Vinput', text: '请输入内容',  radius:2, borderSize: '1', isBorder:'true', borderType:'solid',
        themeBtn_gradient_color:'#ccc', borderColor: '',input_background: '', input_color:''},
        style: {
            width: 250,
            height: 32,
            top: 0,
            left: 0
        },
    },
    {
        component: 'Picture',
        label: '图片',
        icon: 'tupian',
        type:'CommonlyUsed',
        isDialog:true,
        propValue: {value:'https://ejarwebtools.oss-cn-hangzhou.aliyuncs.com/image/2_cf1ca65c7799f893a172206215cf133b.jpg',name:'Vimgae',
        imgwidth:300,
        imgheight:200,
        Alt:'',
        tabIndex:0,
        radius:20,
        opacity:100
        },
        style: {
            width: 400,
            height: 200,
            top:0,
            left:0,
            backgroundColor:'',
        },
    },
    {
        component: 'v-tab',
        label: '导航条',
        propValue:'导航条',
        icon: 'juxing',
        type:'CommonlyUsed',
        tabList:[
            {active:true,label:'首页',link:'https://ffis.me/baidu/index.html?我是sb?'},
            {active:false,label:'解决方案',link:'https://ffis.me/baidu/index.html?我是sb?'},
            {active:false,label:'软件开发',link:'https://ffis.me/baidu/index.html?我是sb?'},
            {active:false,label:'系统集成',link:'https://ffis.me/baidu/index.html?我是sb?'},
            {active:false,label:'关于我们',link:'https://ffis.me/baidu/index.html?我是sb?'},
        ],
        style: {
            width:660,
            height:92,
            fontSize: 18,
            lineHeight: '',
            fontWeight: 500,
            letterSpacing: 0,
            activeColor:'#0061FF',
            color: '',
            borderWidth: 1,
            backgroundColor: '',
        },
    },
    {
        component: 'rect-shape',
        label: '矩形',
        propValue:' ',
        isDialog:true,
        type:'CommonlyUsed',
        icon: 'juxing',
        props:{value: '矩形', name:'RectShape', shapeType:1, headerWidth: '', headerHeight: '58',boxWidth:'',boxHeight:'', radius_top: '',radius_bottom: '', background_top:'#5a75d5',background_bottom:'#fff'},
        style: {
            top: 0,
            left: 0,
            width: 350,
            height: 395,
            fontSize: 14,
            fontWeight: 500,
            lineHeight: '',
            letterSpacing: 0,
            textAlign: 'center',
            textLeft:0,
            textTop:0,
            textRight:0,
            color: '',
            borderColor: '',
            borderWidth: 1,
            // borderBottomWidth: 0,
            // borderBottomColor:'',
            backgroundColor: '',
            borderStyle: 'solid',
            borderRadius: '',
            verticalAlign: 'middle',
            radius: ''
        },
    },
    // {
    //     component: 'v-elcarousel',
    //     label: '轮播图',
    //     propValue:'轮播图',
    //     type:'CommonlyUsed',
    //     swiperList:[
    //         {url:'https://img0.baidu.com/it/u=2806520609,2586054719&fm=253&fmt=auto&app=120&f=JPEG?w=1280&h=800',link:'http://www.baidu.com'},
    //         {url:'https://img0.baidu.com/it/u=2016987350,3161501086&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500',link:'http://www.taobao.com'},
    //     ],
    //     icon: 'button',
    //     style: {
    //         width: 400,
    //         height: 180,
    //         borderRadius: '',
    //     },
    // },
    {
        component: 'v-header',
        label: '网页Header',
        propValue: {value: 'Header',fixed:true,logoleft:0,logotop:0, companyFont:16,corporateName:'',
        activeColor:'#0061FF',activeBg:'#0061FF',companyColor: '#0061FF', imgwidth: 140, imgheight: 30, headerList:[
            {active: false, isChildren:false,Children:[],name: '首页', link: './index.html'},
            {active: false,isChildren:false,Children:[],name: '软件开发', link: './rjkf.html'},
            {active: false,isChildren:false,Children:[],name: '网站建设', link: './wzjs.html'},
            {active: false,isChildren:false,Children:[],name: '项目案例', link: './xmal.html'},
            {active: false,isChildren:true, Children:[
                {name:'智慧物业综合解决方案',link:'./zhwy.html'},
                {name:'校园智慧交通解决方案',link:'./xyzh.html'},
                {name:'酒店软件系统升级方案',link:'./jdrj.html'},
                {name:'数字化档案管理方案',link:'./szhd.html'},
                {name:'视频彩铃',link:'./spcl.html'},
                {name:'智慧交通监管平台',link:'./zhjt.html'},
                {name:'客运车辆远程监控系统解决方案',link:'./kycl.html'},
                {name:'化工厂高精度定位方案',link:'./hgc.html'},
                // {name:'智能售彩解决方案',link:'./znsc.html'},
                {name:'环保法院',link:'./hbfy.html'},
                {name:'县域商贸流通信息化建设方案',link:'./xysm.html'},
                {name:'智慧监狱整体解决方案',link:'./zhjy.html'},
            ],name: '解决方案', link: ''},
            {active: false,isChildren:false,Children:[],name: '关于我们', link: './gywm.html'}
        ],img: 'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/ejar_logo@2x.png', radioType: 2},
        type: 'Basics',
        hoverClass:"",
        isDialog: true,
        icon: 'button',
        style: {
            width: 1920,
            height: 92,
            fontSize: 16,
            color: '#000',
            backgroundColor:'#fff',
            activeColor: '#0061FF',
            fontWeight: 500,
            top: 0,
            left: 0
        }
    },
    {
        component: 'v-footer',
        label: '网页Footer',
        propValue:{value:'Footer',titleType:true,childrenMenu:false,
        childtitle:'解决方案',
        linkTabTop:"59",
        linkTabLeft:"0",
        contentTabTop:"59",
        contentTabLeft:"277",
        lxTabTop:"59",
        lxTabLeft:"637",
        ImgTabLeft:"1100",
        ImgTabTop:"59 ",
        childrenList:[
          {name:'酒店软件系统升级方案',link:'./jdrj.html'},
          {name:'数字化档案管理方案',link:'./szhd.html'},
        ],
        footerList:[
          {name:'首页',link:'./index.html'},
          {name:'软件开发',link:'./rjkf.html'},
          {name:'网站建设',link:'./wzjs.html'},
          {name:'项目案例',link:'./xmal.html'},
          {name:'关于我们',link:'./gywm.html'},
        ],email:'yijiaxunke@ejar.com.cn',phone:'028-85350919',address:'四川省成都市高新区环球中心S2 6-1-2011',erweima:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/微信图片_20220707113401.png',erweimaLabel:'易家讯科官方微信',
        beian:'蜀ICP备19013033号-1',beianlink:'https://beian.miit.gov.cn/#/Integrated/index'},
        type:'Basics',
        isDialog:true,//是否需要弹框进行设置
        icon: 'button',
        style: {
            width: 1920,
            height: 440,
            top:0,
            left:0,
            color: '#fff',
            fontSize:12,
            backgroundColor:'#0B162B',
            borderRadius: '',
        },
    },
    {
        component: 'Listcard',
        label: '列表卡片',
        propValue: {value: 'Listcard', tabIndex:1,
        titleSize:16,
        shadow:true,
        titleColor:'#000',
        titleTop:0,
        titleBold:550,
        titleLeft:0,
        contentTop:0,
        contentLeft:0,
        contentColor:'#000',
        imgwidth:80,
        imgTop:0,
        imgLeft:0,
        imgheight:83,
        contentSize:14,
        title:'产品转型',icon:'',content:`痛点：随着信息时代的到来，竞争白热化，你需要一个软件开发合作伙伴来帮助你实现业务升级与转型？
            解决方案：传统企业信息化转型咨询、落地实施`
        },
        type: 'Basics',
        isDialog: true,
        icon: 'button',
        style:type==1?{
            width: 810,
            height: 124,
            fontSize: 14,
            borderRadius:'',
            color: '#000',
            backgroundColor:'#fff',
            fontWeight: 500,
            top: 0,
            left: 0
        }:{
            width: 343,
            height: 158,
            fontSize: 14,
            borderRadius:'',
            color: '#000',
            backgroundColor:'#fff',
            fontWeight: 500,
            top: 0,
            left: 16
        }
    },
    {
        component: 'Casecard',
        label: '案列卡片',
        propValue: {value: 'Casecard',
        titleSize:16,
        shadow:true,
        imgwidth:380,
        imgheight:256,
        imgTop:0,
        imgLeft:0,
        titleSize:18,
        titleTop:0,
        titleLeft:0,
        titleColor:'#000',
        contentTop:0,
        contentLeft:0,
        contentRight:0,
        contentLineheight:1,
        contentSize:14,
        contentwidth:380,
        contentColor:'#000',
        contentheight:310,
        boxMargin:0,
        flex:'space-around',
        boxShadow:"1",
        boxBgcolor:'#fff',
        list:[
            {
                title:'智慧笔录系统',icon:'https://ejarwebtools.oss-cn-hangzhou.aliyuncs.com/image/1660119152576zhbl.png',content:`案列描述`,link:''
            },
            {
                title:'朝天娇商城',icon:'https://ejarwebtools.oss-cn-hangzhou.aliyuncs.com/image/1660119152576zhbl.png',content:`案列描述`,link:''
            },
            {
                title:'易智宝',icon:'https://ejarwebtools.oss-cn-hangzhou.aliyuncs.com/image/1660119152576zhbl.png',content:`案列描述`,link:''
            },
        ],
        },
        type: 'Basics',
        isDialog: true,
        icon: 'button',
        style: type==1?{
            width: 1141,
            height: 356,
            lineHeight: '',
            fontSize: 14,
            borderRadius:'',
            color: '#000',
            backgroundColor:'#fff',
            textAlign:'center',
            fontWeight: 500,
            top: 0,
            left: 0
        }:{
            width: 373,
            height: 478,
            fontSize: 12,
            borderRadius:'',
            color: '#000',
            backgroundColor:'#fff',
            textAlign:'center',
            fontWeight: 500,
            top: 0,
            left: 0
        }
    },
    {
        component: 'TabSwitchcard',
        label: 'Tab切换卡片',
        propValue: {value: 'TabSwitchcard',
        btnWidth:200,
        btnHeight:60,
        btnActiveColor:'#03004D',
        BtnMargin:8,
        imgwidth:508,
        imgheight:323,
        imgright:105,
        imgleft:0,
        imgTop:0,
        btnAlign:'center',
        itemTop:119,
        Btnradius:10,
        titleColor:'#19224C',
        titleSize:26,
        titleTop:0,
        titleAligin:'flex-start',
        btnitemwidth:120,
        btnitemheight:45,
        btnitemradius:23,
        btnitemSize:18,
        btnitemColor:'#03004D',
        btnbottom:0,
        btnborderColor:'#B7B8FF',
        btnright:36,
        btntop:40,
        btnleft:0,
        rightBoxwidth:630,
        rightBoxmarginLeft:0,
        rightBoxmarginTop:0,
        list:[
            {
                BtnText:'网站模板',img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择',btnlist:['广告','文化传媒','学校','互联网','旅游','餐饮','机械制造','保洁用品','房地产','室内设计','软件','外贸'],
                active:true,inputValue: ''
            },
            {
                BtnText:'商城模板',img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择',btnlist:['广告','文化传媒','学校','互联网','旅游','餐饮','机械制造','保洁用品','房地产','室内设计','软件','外贸'],
                active:false,inputValue: ''
            },
            {
                BtnText:'小程序模板',img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择',btnlist:['广告','文化传媒','学校','互联网','旅游','餐饮','机械制造','保洁用品','房地产','室内设计','软件','外贸'],
                active:false,inputValue: ''
            },
        ],
        },
        type: 'Basics',
        isDialog: true,
        icon: 'button',
        style: {
            width: 1240,
            height: 532,
            fontSize: 14,
            borderRadius:'',
            color: '#000',
            backgroundColor:'',
            textAlign:'center',
            fontWeight: 500,
            top: 0,
            left: 0
        }
    },
    {
        component: 'v-swiper',
        label: '走马灯',
        propValue:{value:'WalkSwiper',tabIndex:0,
        imgwidth:206,
        imgheight:120,
        marginBottom:47,
        marginTop:0,
        swiperList:[
            {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/bogong_logo.png',link:''},
            {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/cywl_logo.png',link:''},
            {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/xnzq_logo.png',link:''},
            {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/zgfp_logo.png',link:''},
            {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/guos_logo.png',link:''},
            {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/linku_logo.png',link:''},
            {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/tfhy_logo.png',link:''},
            {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/west_logo.png',link:''},
        ],slidesPerView: 4,name:'walk',spaceBetween: 10},
        type:'Basics',
        isDialog:true,//是否需要弹框进行设置
        icon: 'button',
        style: {
            width: 932,
            height: 123,
            top:0,
            left:0,
            borderRadius: '',
        },
    },
    //
    {
        component: 'v-swiperinner',
        label: '轮播图',
        type:'CommonlyUsed',
        propValue:'轮播图',
        isContent:true,
        swiperList:[
            {url:'https://img0.baidu.com/it/u=2806520609,2586054719&fm=253&fmt=auto&app=120&f=JPEG?w=1280&h=800',link:'http://www.baidu.com',
            title:'',
            subtitle:'',
            content:'',
            titleStyle:{
                "color":'#fff',
                "fontSize":48,
                "margin-top":0,
                "margin-left":0,
            },
            contentStyle:{
                "color":'#fff',
                "fontSize":28,
                "margin-top":0,
                "paddingAlign":20,
                "borderWidth":1,
                "paddingBetween":45,
                "margin-right":0,
                "margin-left":0,
            },
         },
            {url:'https://img0.baidu.com/it/u=2016987350,3161501086&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500',link:'http://www.taobao.com',title:'',subtitle:'',content:'',
            titleStyle:{
                "color":'#fff',
                "fontSize":48,
                "margin-top":0,
                "margin-left":0,
            },
            contentStyle:{
                "color":'#fff',
                "fontSize":28,
                "margin-top":0,
                "borderWidth":1,
                "paddingAlign":20,
                "paddingBetween":45,
                "margin-left":0,
                "margin-right":0,
            },
        },
        ],
        icon: 'button',
        style: {
            width: 375,
            height: 200,
            borderRadius: '',
        },
    },

    {
        component: 'v-article',
        label: '图文展示',
        propValue:{value:'ImgTextVue',name:'图文展示',tabIndex:0,img:'',imgwidth:450,imgheight:200,content:'<p>图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容图文展示内容</p>'},//value 为标识符 name:标题 tabIndex:默认选中第几个 content:文字内容/富文本
        type:'Basics',
        isDialog:true,//是否需要弹框进行设置
        icon: 'button',
        style: {
            width: 850,
            height: 500,
            top:0,
            left:0,
            borderRadius: '',
            backgroundColor: '#fff',
            color: '',
            borderColor: '#000',
            borderWidth: 1,
            borderStyle: 'solid',
            borderRadius: '',
        },
    },
    {
        component: 'ListMultigraph',
        label: '列表多图',
        propValue:{value:'ListDialog',name:'列表多图',nameKey:'walkLine',imgwidth:230,imgheight:230,tabIndex:0,lineType:2,img:[
            "https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJv17NUFKPLrqr8BMIAPOIAK!160x160.jpg",
            "https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJn17NUFKNvO7dIGMIAPOPcJ!160x160.jpg",
            "https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMCF-r0GMIAPOPcJ!160x160.jpg",
            "https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!160x160.jpg",
        ],
        backgroundImage:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',
        backgroundColor:{left:'rgba(255, 255, 255, 0.6)',right:'#0E5DC4'},
        swiperList:[
            {
                day:'24',
                month:'八月',
                title1:'SHN48',
                title2:'易家讯科',
                text:' 《盛夏好声音》是SNH482015年发行的第八张EP，也是SNH48第二张总选投票专辑，共收录了《盛夏好声音》、《自我主张》、《亲吻进行式》、《眼神加速度》、《清纯哲学》5首歌曲 ',
                img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',
            },
            {
                day:'24',
                month:'八月',
                title1:'SHN48',
                title2:'易家讯科',
                text:' 《盛夏好声音》是SNH482015年发行的第八张EP，也是SNH48第二张总选投票专辑，共收录了《盛夏好声音》、《自我主张》、《亲吻进行式》、《眼神加速度》、《清纯哲学》5首歌曲 ',
                img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',
            }
        ]},
        type:'Basics',
        isDialog:true,//是否需要弹框进行设置
        icon: 'button',
        style: {
            width: 1020,
            height: 450,
            top:0,
            left:0,
        }
    },
    {
        component: 'v-articlelist',
        label: '文章列表',
        propValue:{ value: 'ArticleList',
                    name: '文章列表',
                    tabIndex: 0,
                    HeadlineStyle: 1,
                    lineType: 1,
                    lineType1: 1,
                    lineType2: 1,
                    HeadlineStyle3: 1,
                    lineType: 1,
                    img:'',
                    content: '<p>我是一个敢冒险、守纪律、保守、热情、富有想像力、害羞、独立、理性、自尊心高和没有信心的人。我想追求幸福，因为想过着令人羡慕的生活；也想追求刺激，因为想要过着精彩又美好的生活；还想要追求快乐，因为我不想要天天摆一张臭脸，想要每天快快乐乐地过日子；最重要的，是想要追求健康，因为我害怕动手术，想要过着没有病痛的日子，让我安心，丝毫不会感到害怕。</p>'},
        type:'Basics',
        isDialog:true,//是否需要弹框进行设置
        icon: 'button',
        style: {
            width: 1200,
            height: 180,
            top:0,
            left:0,
        }
    },
    {
        component: 'AtlasCatalogue',
        label: '图册目录',
        propValue: {value: 'AtlasDialog', name: '图册目录', tabIndex: 0,fontSize1:12,
        backgroundColor:{left:'',right:''},
        titleSize:16,
        titleColor:'#fff',
        subtitleColor:'#fff',
        boxRadius:0,
        subtitleSize:14,
        titleTop:0,
        titleLeft:0,
        contentTop:0,
        contentLeft:0,
        imgMargin:0,
        imgList: [
            {img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',title: '这是第一张图片', content: '这是图片的内容'},
            {img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',title: '这是第二张图片', content: '这是图片的内容'},
            {img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',title: '这是第三张图片', content: '这是图片的内容'},
            {img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',title: '这是第四张图片', content: '这是图片的内容'},
            {img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',title: '这是第五张图片', content: '这是图片的内容'},
            {img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',title: '这是第六张图片', content: '这是图片的内容'},
            {img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',title: '这是第七张图片', content: '这是图片的内容'},
            {img:'https://1.s140i.faiusr.com/2/AIwBCAAQAhgAIJr17NUFKMHkwcsBMIAPOIAK!300x300.jpg.webp',title: '这是第八张图片', content: '这是图片的内容'},
        ],textType: '', boxwidth: 200, boxheight: 300},
        type: 'Basics',
        isDialog: true,
        icon: 'button',
        style: {
            width: 800,
            height: 610,
            color:'#fff',
            textAlign:'center',
            fontSize: 14,
            fontWeight: 500,
            borderRadius: '',
            top: 0,
            left: 0,
        }
    },
    {
        component: 'Onlinevideo',
        label: '在线视频',
        propValue: {value: '在线视频', name: 'OnlinevideoDialog',VideoType:1,posterType:false,poster:'',videoUrl:'https://apd-bea7cad500ab8b75aa8dd55097890dfb.v.smtcdns.com/music.qqvideo.tc.qq.com/A21iztMYIcfiQmv0EXeCFYsKVZYbDz0-eSpt06AOovxg/uwMROfz2r5zCIaQXGdGnC2dfDmavB6NbsWKJAiRTQBPbT70a/t0032kwa29w.mp4?sdtfrom=v5010&guid=056a42b75357670e&vkey=76ED652B6D9981065C8A235F870A9A6214D2157BF280148928A8B8CDC55ACD5EDF602D0FB5CAC7EC0E2292D93AD87818527326B430F55E90E2333611F9D8606CDB1FB6058E0A1DD74397C4648337EC6EC7263671BF2DA90AB978E985A2B1DDAABC7B2B2D84B705F423235BD113287CBFA7875AF9BB9E096041B9BA470B7F3870',videoWidth:600,videoHeight:300,loop:false,autoplay:false,controls:true},
        type: 'CommonlyUsed',
        isDialog: true,
        icon: 'button',
        style: {
            width: 600,
            height: 300,
            top: 0,
            left: 0,
        }
    },
    {
        component: 'v-from',
        label: '在线表单',
        propValue: {value: 'FromSubmit',fromKey:'',name: '联系我们',tabIndex:0,
        FixedTop:80,
        FixedLeft:2,
        fromData:[
            {
                placeholder: "姓名",
                label: "姓名",
                value:'',
                key:'realName',
                radio: true,
                icon: '',
                rule: '',
              },
              {
                placeholder: "电话（必填）",
                label: "电话",
                value:'',
                key:'phone',
                radio: true,
                icon: '',
                rule: '',
              },
              {
                placeholder: "邮箱",
                label: "邮箱",
                value:'',
                key:'mail',
                radio: true,
                icon: '',
                rule: '',
              },
              {
                placeholder: "在此处输入内容，我们会尽快与您联系",
                label: "",
                value:'',
                key:'message',
                radio: false,
                icon: '',
                rule: '',
              },
        ]},
        type: 'Basics',
        isDialog: true,
        icon: 'button',

        style: {
            width: 300,
            height: 60,
            fontWeight: 500,
            color:'#fff',
            borderRadius: '',
            backgroundColor: 'rgba(255, 255, 255, 0)',
            // top: 0,
            // left: 0,
        }
    },
    {
        component: 'v-advantage',
        label: '优势',
        propValue: {value: 'Advantage',name: '优势', titleSize: 16, contentSize: 14, titleWeight: 500, contentWeight: 500, backgroundColor: '#fff',
        paddingTransverse:0,paddingVertical:0,
        width: 416, height: 268, borderRadius:'0rem 0rem 0rem 0rem', iconMarginTop: '0',iconMarginLeft: '0', titleMarginTop: '0',titleMarginLeft: '0 ',
        boxMargin:0,
        contentMarginTop: '0', contentMarginLeft: '0', contentMarginRight: '0', contentPaddingLeft: '0', contentPaddingRight: '0', flex: 'flex-start',
        titleColor:'#000',
        contentColor:'#000',
        shadow:false,
        imgwidth:50,
        imgheight:50,
        imgList: [
            {img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/advantage_ksxy@2x.png',title: '高效研发', content: '这是图片的内容'},
            {img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/advantage_gxyf@2x.png',title: '专业', content: '这是图片的内容'},
            {img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/advantage_aqkk@2x.png',title: '团队', content: '这是图片的内容'},
            {img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/advantage_gxxt@2x.png',title: '经验', content: '这是图片的内容'},
            {img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/advantage_dcb@2x.png',title: '服务', content: '这是图片的内容'},
            {img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/advantage_gzl@2x.png',title: '规范', content: '这是图片的内容'},
        ],},
        type: 'Basics',
        isDialog: true,
        icon: 'button',
        style: {
            width: 1440,
            height: 680,
            textAlign:'center',
            top: 0,
            left: 0,
        }
    },
    {
        component: 'v-hover',
        label: '移入效果',
        propValue: {value: 'Hover',name: '移入效果',imgwidth: 80, imgheight: 80,  textsize: 20, radio: 1,borderRadius:12,
        imgList: [
            {img:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/advantage_ksxy@2x.png',title: '高效研发', content: '这是图片的内容'},
        ],
        },

        type: 'Basics',
        isDialog: true,
        icon: 'button',
        style: {
            width: 219,
            height: 263,
            top: 0,
            left: 0,
        }
    },
    {
        component: 'TabImgSwitchcard',
        label: 'Tab图片卡片',
        propValue: {value: 'TabImgSwitchcard',
        btnWidth:160,
        btnHeight:60,
        btnActiveColor:'#0061FF',
        BtnMargin:8,
        BtnSize:24,
        BtnMiddleMargin:25,
        imgwidth:380,
        imgheight:246,
        itemBackground:'#fff',
        imgright:0,
        imgleft:0,
        imgTop:0,
        imgBottom:0,
        btnAlign:'center',
        itemTop:100,
        contentInnerBottom:54,
        titleSize:20,
        ContentSize:14,
        titleColor:'#222',
        titleAlignPadding:13,
        titleBetweenPadding:5,
        contentAlignPadding:13,
        contentBetweenPadding:5,
        ContentColor:'#222',
        boxShadow:true,
        themeBtnClass:'TabImgSwitchcard_box',
        titleAligin:'center',
        shadowColor:'#ccc',
        FontcontentAlign:'center',
        titleTop:0,
        contentFlex:'space-between',
        list:[
            {
                title:'网站模板1',imgList:[
                    {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择1',link:'',content:''},
                    {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择1',link:'',content:''},
                    {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择1',link:'',content:''},
                    {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择1',link:'',content:''},
                    {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择1',link:'',content:''},
                    {url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择1',link:'',content:''},

                ],
                active:true,
            },
            {
                title:'网站模板2',imgList:[{url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择2',link:'',content:''}],
                active:false,
            },
            {
                title:'网站模板3',imgList:[{url:'https://ejartest.oss-cn-chengdu.aliyuncs.com/image/muban_pic.png',title:'全场景网站模板提供选择3',link:'',content:''}],
                active:false,
            },
        ],
        },
        type: 'Basics',
        isDialog: true,
        icon: 'button',
        style: {
            width: 1200,
            height: 830,
            fontSize: 14,
            borderRadius:'',
            color: '#000',
            backgroundColor:'',
            textAlign:'center',
            fontWeight: 500,
            top: 0,
            left: 0
        }
    },
 ...phoneConponentsList,
 ...customDataList

]

for (let i = 0, len = list.length; i < len; i++) {
    const item = list[i]
    item.style = { ...commonStyle, ...item.style }
    list[i] = { ...commonAttr, ...item }
}

export default list
