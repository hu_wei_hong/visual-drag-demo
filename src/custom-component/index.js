import Vue from 'vue'

const components = [
    'Picture',
    'VText',
    'VButton',
    'Group',
    'VInput',
    'RectShape',
    // 'VElcarousel',
    "VSwiper",
    "VSwiperinner",
    "VTab",
    "VArticle",
    "ListMultigraph",
    "VArticlelist",
    "AtlasCatalogue",
    "Onlinevideo",
    "VFooter",
    "VHeader",
    "VFrom",
    "Listcard",
    "Casecard",
    "VAdvantage",
    "TabSwitchcard",
    "TabImgSwitchcard",
    "VHover",
]

const mobileComponents=[
    "MobileHeader",
    "MobileFooter",
    "MobileFrom",
    "MobileSwitchcard",
    "MobileScrollX"
]
components.forEach(key => {
    Vue.component(key, () => import(`@/custom-component/${key}`))
})

mobileComponents.forEach(key => {
    Vue.component(key, () => import(`@/custom-component/mobileComponents/${key}`))
})
