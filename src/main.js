import Vue from 'vue'
import ElementUI from 'element-ui'
import App from './App'
import store from './store'
import router from './router'
import '@/custom-component' // 注册自定义组件
import '@/utils/dialogDrag'// 全局引入v-dialogDrag自定义指令
import '@/assets/iconfont/iconfont.css'
import '@/styles/animate.scss'
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/reset.css'
import './icons' // icon

Vue.prototype.$uploadUrl='http://192.168.5.206:8842/client/common/uploadFile'
Vue.use(ElementUI, { size: 'small' })
Vue.config.productionTip = false
Vue.prototype.$EventBus = new Vue()
Vue.prototype.$BaseUrl = localStorage.getItem('baseUrl')
Vue.directive('drag', {
    inserted (el, binding) {
        if(window.location.href.includes('preview')){return}
      let oDiv = el
  //  左边距最大值
      let maxLeft = el.parentNode.clientWidth - el.clientWidth
  //  上边距最大值
      let maxTop = el.parentNode.clientHeight - el.clientHeight
      oDiv.onmousedown = function (e) {
  //  鼠标按下，计算当前元素距离可视区的距离
        let disX = e.clientX - oDiv.offsetLeft
        let disY = e.clientY - oDiv.offsetTop
        document.onmousemove = function (e) {
  //  获取到鼠标拖拽后的横向位移(距离父级元素)
          let l = e.clientX - disX
  //  获取到鼠标拖拽后的纵向位移(距离父级元素)
          let t = e.clientY - disY 
          oDiv.style.left = l + 'px' 
          oDiv.style.top = t + 'px'
          if (e.clientX - disX <= 0) { oDiv.style.left = 0 + 'px' } 
          if (e.clientY - disY <= 0) { oDiv.style.top = 0 + 'px' } 
          if (e.clientX - disX >= maxLeft) { oDiv.style.left = maxLeft + 'px' } 
          if (e.clientY - disY >= maxTop) { oDiv.style.top = maxTop + 'px' } 
  //     将此时的位置传出去  
          binding.value({x: oDiv.style.left, y: oDiv.style.top}) } 
  //     松开事件后，移除事件
         document.onmouseup = function (e) { 
            document.onmousemove = null 
            document.onmouseup = null 
        } 
       } 
     } 
  })
console.log(localStorage.getItem('baseUrl'))
// localStorage.setItem('baseUrl',process.env.VUE_APP_BASE_API)
// console.log(process.env.VUE_APP_BASE_API)
// console.log(process.env.NODE_ENV)

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
})
