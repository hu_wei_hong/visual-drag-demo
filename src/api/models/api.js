import http from '@/utils/request.js'
export default {
    // 接口：查询模板列表
    getTemplateList:(params = {}) => http.post("/template/getTemplateList", params),
    addTemplate:(params = {}) => http.post("/template/addTemplate", params),
    saveTemplate:(params = {}) => http.post("/template/saveTemplate", params),
    updateTemplate:(params = {}) => http.post("/template/updateTemplate", params),
    templatePasswordVerification:(params = {}) => http.post("/template/templatePasswordVerification", params),
    deleteTemplate:(params = {}) => http.post("/template/deleteTemplate", params),
    addComponentsAutomatically:(params = {}) => http.post("/componentsAutomatically/addComponentsAutomatically", params),
    getComponentsAutomaticallyList:(params = {}) => http.post("/componentsAutomatically/getComponentsAutomaticallyList", params),
    deleteComponentsAutomatically:(params = {}) => http.post("/componentsAutomatically/deleteComponentsAutomatically", params),
}